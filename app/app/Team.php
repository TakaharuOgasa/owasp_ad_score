<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $guarded = ['id', 'created_at'];
    static public $validateRule = [
        'name' => 'required',
        'color' => 'required'
    ];

    public function generateToken()
    {
        $this->token = sha1(uniqid(mt_rand()));
    }
}

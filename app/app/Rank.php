<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;

class Rank extends Model
{
    protected $guarded = ['id', 'created_at'];

    static public $validateRule = [
        '' => 'required',
        'rank' => 'required|integer',
        'team_id' => 'required|integer',
        'score' => 'required|integer',
        'count' => 'required|integer'
    ];

    public static function getLastRank()
    {
        $last = Rank::orderBy('id', 'desc')->first();
        if(is_null($last))
        {
            return 0;
        }
        else
        {
            return $last->count;
        }
    }

    public static function getLastRanks()
    {
        $last = Rank::orderBy('id', 'desc')->first();
        if(is_null($last))
        {
            return null;
        }
        else
        {
            return \DB::table('ranks')
            ->select('ranks.rank', 'teams.name', 'teams.color', 'ranks.score')
            ->where('ranks.count', $last->count)
            ->leftJoin('teams','team_id','=','teams.id')
            ->orderBy('ranks.rank')
            ->get();
        }
    }

    public static function Calc($count)
    {
        $ranks = Rank::where('count', $count)
        ->orderBy('score', 'desc')
        ->get();

        $count = 0;
        $score = -1;
        foreach($ranks as $rank)
        {
            if($score != $rank->score)
            {
                $count++;    
            }
            $score = $rank->score;
            $rank->rank = $count;
            $rank->save();
        }
    }

    public function getRealTime()
    {
        return date('Y-m-d H:i:s', strtotime(Config::get('sla.start')) + ($this->count * Config::get('sla.iterate') * 60));
    }
}

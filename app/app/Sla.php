<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sla extends Model
{
    protected $guarded = ['id', 'created_at'];
    static public $validateRule = [
        'name' => 'required',
        'point' => 'required|integer'
    ];

    protected $attributes = [
        'is_enabled' => 0, 
        'body' => ''
    ];
}

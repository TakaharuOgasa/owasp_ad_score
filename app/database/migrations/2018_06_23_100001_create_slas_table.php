<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlasTable extends Migration
{
    public function up()
    {
        Schema::create('slas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('body');
            $table->integer('point');
            $table->tinyInteger('is_enabled');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('slas');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlalogsTable extends Migration
{
    public function up()
    {
        Schema::create('slalogs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sla_id');
            $table->integer('team_id');
            $table->tinyInteger('is_success');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('slalogs');
    }
}
